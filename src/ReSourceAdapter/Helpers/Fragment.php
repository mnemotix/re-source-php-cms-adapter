<?php
/**
 * This file is part of the lafayette-anticipations package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/04/2017
 */

namespace ReSourceAdapter\Helpers;


class Fragment {
  static $incr = 0;

  public static function generateName($prefix = 'F'){
    self::$incr++;

    return $prefix . self::$incr;
  }
}

<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;

class Organization extends Actor {
  /** @var string Organization short name */
  protected $shortName;

  /** @var string Organization description */
  protected $description;

  /** @var string Organization short description */
  protected $shortDescription;

  /** @var \ReSourceAdapter\Model\Affiliation[] members */
  protected $members = [];

  /**
   * @return string
   */
  public function getShortName() {
    return $this->shortName;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return string
   */
  public function getShortDescription() {
    return $this->shortDescription;
  }

  /**
   * @return \ReSourceAdapter\Model\Affiliation[]
   */
  public function getMembers() {
    return $this->members;
  }

  /**
   * Get organization GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $affiliationFragmentName = Fragment::generateName();
    $affiliationFragment = Affiliation::getFragment($affiliationFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on Organisation{
  id
  displayName
  description
  shortDescription
  shortName
  avatar
  creationDate
  lastUpdate
  members: affiliations(first: 100){
    edges{
      affiliation: node{
        ...$affiliationFragmentName
      }
    }
  }
  externalLinks{
    edges{
      externalLink: node{
        name
        link
      }
    }
  }
}

$affiliationFragment
GRAPHQL;
  }

  /**
   * Get organisation from GraphQL response data.
   * @param $data
   * @return \ReSourceAdapter\Model\Organization
   */
  static function fromResponse($data) {
    $data = $data['organisation'];

    $organization = new Organization();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'members':
          $organization->members = [];
          if(isset($value)) {
            foreach ($value['edges'] as $memberNode){
              $organization->members[] = Affiliation::fromResponse($memberNode);
            }
          }
          break;
        case 'externalLinks':
          if(isset($value)) {
            foreach ($value['edges'] as $externalLinkNode){
              $externalLink = ExternalLink::fromResponse($externalLinkNode);

              switch ($externalLink->getName()) {
                case 'facebook':
                  $organization->facebookUri = $externalLink->getLink();
                  break;
                case 'twitter':
                  $organization->twitterUri = $externalLink->getLink();
                  break;
                case 'instagram':
                  $organization->instagramUri = $externalLink->getLink();
                  break;
                default:
                  $organization->websiteUri = $externalLink->getLink();
                  break;
              }
            }
          }
          break;
        default:
          $organization->{$property} = $value;
      }
    }

    return $organization;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    $parentJSON = parent::jsonSerialize();

    return array_merge($parentJSON,[
      'description' => $this->getDescription(),
      'shortName' => $this->getShortName(),
      'members' => array_map(function($member){return $member->jsonSerialize(); }, $this->getMembers()),
      'shortDescription' => $this->getShortDescription()
    ]);
  }
}

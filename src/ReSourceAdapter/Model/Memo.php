<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;

class Memo extends ModelAbstract {
  /** @var \ReSourceAdapter\Model\Person Memo author */
  protected $author;

  /** @var string Memo content */
  protected $content;

  /** @var string Memo creation date */
  protected $creationDate;

  /** @var string Memo last update */
  protected $lastUpdate;

  /**
   * @return \ReSourceAdapter\Model\Person
   */
  public function getAuthor() {
    return $this->author;
  }

  /**
   * @return string
   */
  public function getContent() {
    return $this->content;
  }

  /**
   * @return string
   */
  public function getCreationDate() {
    return $this->creationDate;
  }

  /**
   * @return string
   */
  public function getLastUpdate() {
    return $this->lastUpdate;
  }

  /**
   * Get resource GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $personFragmentName = Fragment::generateName();
    $personFragment = Person::getFragment($personFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on Memo{
  id
  content
  author: creator{
    ...$personFragmentName
  }
  creationDate
  lastUpdate
}

$personFragment
GRAPHQL;
  }

  /**
   * Get resource GraphQL query
   *
   * @param $memoId
   * @return string
   */
  static function getQuery($memoId){
    $fragmentName = Fragment::generateName();
    $fragment = self::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  memo(memoId:"$memoId") {
    ...$fragmentName
  }
}

$fragment
GRAPHQL;
  }

  /**
   * Get event from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Memo
   */
  static function fromResponse($data) {
    $data = $data['memo'];

    $resource = new Memo();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'author':
          $resource->author = Person::fromResponse(['person' => $value]);
          break;
        default:
          $resource->{$property} = $value;
      }
    }

    return $resource;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'content' => $this->getContent(),
      'author' => $this->getAuthor()->jsonSerialize(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate()
    ];
  }
}

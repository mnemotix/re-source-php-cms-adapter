<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;

abstract class Project extends ModelAbstract {
  static $extraFragment = "";

  /** @var string Project title */
  protected $title;

  /** @var string Project description */
  protected $description;

  /** @var string Project short description */
  protected $shortDescription;

  /** @var \ReSourceAdapter\Model\Actor[] Project authors */
  protected $authors = [];

  /** @var \ReSourceAdapter\Model\Involvement[] Project involvements */
  protected $involvements = [];

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return string
   */
  public function getShortDescription() {
    return $this->shortDescription;
  }

  /**
   * @return \ReSourceAdapter\Model\Actor[]
   */
  public function getAuthors() {
    return $this->authors;
  }

  /**
   * @return \ReSourceAdapter\Model\Involvement[]
   */
  public function getInvolvements() {
    return $this->involvements;
  }

  /**
   * Get extra fragment
   * @param $fragmentName
   * @return string
   */
  static function getExtraFragment($fragmentName){
    return <<<GRAPHQL
fragment $fragmentName on Project{
  id
}
GRAPHQL;
  }

  /**
   * Get person GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $projectAuthorFragmentName = Fragment::generateName();
    $projectAuthorFragment = Actor::getFragment($projectAuthorFragmentName);

    $projectInvolvementFragmentName = Fragment::generateName();
    $projectInvolvementFragment = Involvement::getFragment($projectInvolvementFragmentName);


    $extraFragmentFragmentName = Fragment::generateName();
    $extraFragment = static::getExtraFragment($extraFragmentFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on Project{
  id
  title
  description
  shortDescription
  creationDate
  lastUpdate
  productionDate
  dimension
  technicalInfos
  collection
  startDate
  endDate
  authors{
    edges{
      actor: node{
        ...$projectAuthorFragmentName
      }
    }
  }
  involvements{
    edges{
      involvement: node{
        ...$projectInvolvementFragmentName
      }
    }
  }
  ...$extraFragmentFragmentName
}

$projectAuthorFragment
$projectInvolvementFragment
$extraFragment
GRAPHQL;
  }

  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
      'shortDescription' => $this->getShortDescription(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate(),
      'involvements' => array_map(function($involvement){return $involvement->jsonSerialize(); }, $this->getInvolvements()),
      'authors' => array_map(function($author){return $author->jsonSerialize(); }, $this->getAuthors())
    ];
  }
}

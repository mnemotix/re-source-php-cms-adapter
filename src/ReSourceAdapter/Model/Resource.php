<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;

class Resource extends ModelAbstract {
  /** @var string Resource title */
  protected $title;

  /** @var string Resource description */
  protected $description;

  /** @var string Resource Uri */
  protected $uri;

  /** @var string Resource credits */
  protected $credits;

  /** @var string Resource type mime */
  protected $mime;

  /** @var string Resource size */
  protected $size;

  /** @var \ReSourceAdapter\Model\ExternalLink[] Resource external links */
  protected $externalLinks = [];


  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return string
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * @return string
   */
  public function getCredits() {
    return $this->credits;
  }

  /**
   * @return string
   */
  public function getMime() {
    return $this->mime;
  }

  /**
   * @return string
   */
  public function getSize() {
    return $this->size;
  }

  /**
   * @return \ReSourceAdapter\Model\ExternalLink[]
   */
  public function getExternalLinks() {
    return $this->externalLinks;
  }

  /**
   * Get resource GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    return <<<GRAPHQL
fragment $fragmentName on Resource{
  id
  title
  description
  credits
  uri: downloadUrl
  creationDate
  lastUpdate
  mime
  size
  externalLinks{
    edges{
      externalLink: node{
        name
        link
      }
    }
  }
}
GRAPHQL;
  }

  /**
   * Get resource GraphQL query
   *
   * @param $resourceId
   * @return string
   */
  static function getQuery($resourceId){
    $fragmentName = Fragment::generateName();
    $fragment = self::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  resource(resourceId:"$resourceId") {
    ...$fragmentName
  }
}

$fragment
GRAPHQL;
  }

  /**
   * Get event from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Resource
   */
  static function fromResponse($data) {
    $data = $data['resource'];

    $resource = new Resource();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'externalLinks':
          if(isset($value)) {
            foreach ($value['edges'] as $externalLinkNode){
              $resource->externalLinks[] = ExternalLink::fromResponse($externalLinkNode);
            }
          }
          break;
        default:
          $resource->{$property} = $value;
      }
    }

    return $resource;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
      'uri' => $this->getUri(),
      'credits' => $this->getCredits(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate(),
      'mime' => $this->getMime(),
      'size' => $this->getSize(),
      'externalLinks' => array_map(function($externalLink){return $externalLink->jsonSerialize(); }, $this->getExternalLinks())
    ];
  }
}

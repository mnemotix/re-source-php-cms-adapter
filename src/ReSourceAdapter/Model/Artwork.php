<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;
use ReSourceAdapter\Helpers\ListQueryParams;

class Artwork extends ModelAbstract {
  /** @var string Artwork title */
  protected $title;

  /** @var string Artwork description */
  protected $description;

  /** @var string Artwork short description */
  protected $shortDescription;

  /** @var \ReSourceAdapter\Model\Actor[] Artwork authors */
  protected $authors = [];

  /** @var \ReSourceAdapter\Model\Event[] Artwork actors */
  protected $events = [];

  /** @var  int Artwork production date  */
  protected $year;

  /** @var  string Artwork dimension */
  protected $dimension;

  /** @var  string Artwork technical information */
  protected $technicalInfos;

  /** @var  string Artwork collection */
  protected $collection;

  /** @var string Artwork courtesy */
  protected $courtesy;

  /** @var string Artwork credits */
  protected $credits;

  /** @var \ReSourceAdapter\Model\Resource[] Artwork pictures */
  protected $pictures = [];

  /** @var \ReSourceAdapter\Model\Resource Artwork credits */
  protected $mainPicture;

  /**
   * @return \ReSourceAdapter\Model\Event[]
   */
  public function getEvents() {
    return $this->events;
  }

  /**
   * @return int
   */
  public function getYear() {
    return $this->year;
  }

  /**
   * @return string
   */
  public function getDimension() {
    return $this->dimension;
  }

  /**
   * @return string
   */
  public function getTechnicalInfos() {
    return $this->technicalInfos;
  }

  /**
   * @return string
   */
  public function getCollection() {
    return $this->collection;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return string
   */
  public function getShortDescription() {
    return $this->shortDescription;
  }

  /**
   * @return \ReSourceAdapter\Model\Actor[]
   */
  public function getAuthors() {
    return $this->authors;
  }

  /**
   * @return string
   */
  public function getCourtesy() {
    return $this->courtesy;
  }

  /**
   * @return string
   */
  public function getCredits() {
    return $this->credits;
  }

  /**
   * @return \ReSourceAdapter\Model\Resource[]
   */
  public function getPictures() {
    return $this->pictures;
  }

  /**
   * @return \ReSourceAdapter\Model\Resource
   */
  public function getMainPicture() {
    return $this->mainPicture;
  }

  /**
   * Get extra fragment
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $authorFragmentName = Fragment::generateName();
    $authorFragment = Actor::getFragment($authorFragmentName);

    $resourceFragmentName = Fragment::generateName();
    $resourceFragment = Resource::getFragment($resourceFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on ArtworkObject{
  id
  title
  description
  shortDescription
  creationDate
  lastUpdate
  dimension: dimensions
  year
  technicalInfos
  collection
  courtesy
  credits
  pictures {
    edges {
      resource: node {
        ...$resourceFragmentName
      }
    }
  }
  mainPicture {
    ...$resourceFragmentName
  }
  authors{
    edges{
      actor: node{
        ...$authorFragmentName
      }
    }
  }
}

$authorFragment
$resourceFragment
GRAPHQL;
  }

  /**
   * Get exhibition artwork list GraphQL query.
   *
   * @param $exhibitionId
   * @param \ReSourceAdapter\Helpers\ListQueryParams $args
   * @return string
   */
  static function getListQuery($exhibitionId, ListQueryParams $args){
    $fragmentName = Fragment::generateName();
    $fragment = Artwork::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  exhibition: artisticObject(artisticObjectId: "$exhibitionId") {
    ...on ExhibitionObject{
      artworks({$args->graphQLize()}){
        edges{
          artwork: node{
            ...$fragmentName
          }
        }
      }
    }
  }
}

$fragment

GRAPHQL;
  }

  /**
   * Return a list of artworks from a GraphQL response.
   *
   * @param array $data
   * @return \ReSourceAdapter\Model\Artwork[]
   */
  static function fromListResponse(array $data){
    $artworks = [];

    foreach ($data['exhibition']['artworks']['edges'] as $artworksData) {
      $artworks[] = Artwork::fromResponse($artworksData);
    }

    return $artworks;
  }


  /**
   * Get artwork from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Artwork
   */
  static function fromResponse($data) {
    $data = $data['artwork'];

    $artwork = new Artwork();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'events':
          $artwork->events = [];

          if(isset($value)) {
            foreach ($value['edges'] as $eventNode){
              $artwork->events[] = Event::fromResponse($eventNode);
            }
          }
          break;

        case 'authors':
          $artwork->authors = [];
          if(isset($value)) {
            foreach ($value['edges'] as $authorNode){
              $artwork->authors[] = Actor::fromResponse($authorNode);
            }
          }
          break;

        case 'pictures':
          $artwork->pictures = [];

          if(isset($value)) {
            foreach ($value['edges'] as $pictureNode){
              $artwork->pictures[] = Resource::fromResponse($pictureNode);
            }
          }
          break;

        case 'mainPicture':
          if(isset($value)) {
            $artwork->mainPicture = Resource::fromResponse(["resource" => $value]);
          }
          break;

        case 'externalLinks':
          break;

        default:
          $artwork->{$property} = $value;
      }
    }

    return $artwork;
  }

  /**
   * Get exhibition GraphQL query.
   *
   * @param $artworkId
   * @param string $projectAlias
   * @return string
   */
  static function getQuery($artworkId){
    $fragmentName = Fragment::generateName();
    $fragment = Artwork::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  artwork: artisticObject(artisticObjectId:"$artworkId") {
    ...$fragmentName
  }
}

$fragment
GRAPHQL;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    $project = parent::jsonSerialize();

    return array_merge($project, [
      'id' => $this->getId(),
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
      'shortDescription' => $this->getShortDescription(),
      'collection' => $this->getCollection(),
      'year' => $this->getYear(),
      'technicalInfos' => $this->getTechnicalInfos(),
      'dimension' => $this->getDimension(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate(),
      'courtesy' => $this->getCourtesy(),
      'credits' => $this->getCredits(),
      'pictures' => array_map(function($resource){return $resource->jsonSerialize(); }, $this->getPictures()),
      'mainPicture' => $this->getMainPicture() ? $this->getMainPicture()->jsonSerialize() : null,
      'authors' => array_map(function($author){return $author->jsonSerialize(); }, $this->getAuthors()),
      'events' => array_map(function($event){return $event->jsonSerialize(); }, $this->getEvents()),
    ]);
  }
}

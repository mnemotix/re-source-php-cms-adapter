<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;

class Affiliation extends ModelAbstract {
  /** @var \ReSourceAdapter\Model\Actor Involvement actor */
  protected $person;

  /** @var  string Involvement role */
  protected $role;

  /**
   * @return \ReSourceAdapter\Model\Actor
   */
  public function getPerson() {
    return $this->person;
  }

  /**
   * @return string
   */
  public function getRole() {
    return $this->role;
  }

  /**
   * Get organization GraphQL fragment.
   *
   * @param $fragmentName
   * @param bool $fromPerson
   * @return string
   */
  static function getFragment($fragmentName, $includePersonFragment = true, $includeOrganisationFragment = true){
    $personFragmentName = Fragment::generateName();
    $personFragment = Person::getFragment($personFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on Affiliation{
  id
  role
  person{
    ...$personFragmentName
  }
}

$personFragment
GRAPHQL;
  }


  /**
   * Get affiliation from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Affiliation
   */
  static function fromResponse($data) {
    $data = $data['affiliation'];

    $affiliation = new Affiliation();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'person':
          $affiliation->person = Person::fromResponse(['person' => $value]);
          break;
        default:
          $affiliation->{$property} = $value;
      }
    }

    return $affiliation;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'role' => $this->getRole(),
      'person' => $this->getPerson()->jsonSerialize()
    ];
  }
}

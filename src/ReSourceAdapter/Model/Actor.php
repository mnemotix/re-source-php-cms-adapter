<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Cursor;
use ReSourceAdapter\Helpers\Fragment;
use ReSourceAdapter\Helpers\ListQueryParams;

abstract class Actor extends ModelAbstract {
  /** @var string Actor name */
  protected $displayName;

  /** @var  string Avatar image Uri */
  protected $avatar;

  /** @var string Actor facebook Uri */
  protected $facebookUri;

  /** @var string Actor twitter Uri */
  protected $twitterUri;

  /** @var string Actor instagram Uri */
  protected $instagramUri;

  /** @var string Actor website Uri */
  protected $websiteUri;

  /**
   * Get actor name
   * @return mixed
   */
  public function getDisplayName() {
    return $this->displayName;
  }

  /**
   * Get actor avatar
   * @return string
   */
  public function getAvatar() {
    return $this->avatar;
  }

  /**
   * @return string
   */
  public function getFacebookUri() {
    return $this->facebookUri;
  }

  /**
   * @return string
   */
  public function getTwitterUri() {
    return $this->twitterUri;
  }

  /**
   * @return string
   */
  public function getInstagramUri() {
    return $this->instagramUri;
  }

  /**
   * @return string
   */
  public function getWebsiteUri() {
    return $this->websiteUri;
  }

  /**
   * Get person GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $personFragmentName = Fragment::generateName();
    $personFragment = Person::getFragment($personFragmentName);

    $orgFragmentName = Fragment::generateName();
    $orgFragment = Organization::getFragment($orgFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on ActorInterface{
    __typename
    id
    displayName
    avatar
    creationDate
    lastUpdate
    ...$personFragmentName
    ...$orgFragmentName
}

$personFragment
$orgFragment
GRAPHQL;
  }

  /**
   * Get organization GraphQL query.
   *
   * @param $actorId
   * @return string
   */
  static function getQuery($actorId){
    $fragmentName = Fragment::generateName();
    $fragment = self::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  actor(actorId: "$actorId") {
    ...$fragmentName
  }
}

$fragment
GRAPHQL;
  }

  /**
   * Get actors list GraphQL query.
   *
   * @param \ReSourceAdapter\Helpers\ListQueryParams $args
   * @return string
   */
  static function getListQuery(ListQueryParams $args){
    $fragmentName = Fragment::generateName();
    $fragment = self::getFragment($fragmentName);

    $pageInfo = Cursor::getPageInfoFragment();

    return <<<GRAPHQL
query{
  actors({$args->graphQLize()}) {
    edges{
      actor: node{
        ...$fragmentName
      }
    }
    $pageInfo
  }
}

$fragment

GRAPHQL;
  }

  /**
   * Return a list of Actor from a GraphQL response.
   *
   * @param array $data
   * @return \ReSourceAdapter\Model\Actor[]
   */
  static function fromListResponse(array $data){
    $actors = [];

    foreach ($data['actors']['edges'] as $actorData) {
      $actors[] = self::fromResponse($actorData);
    }

    return $actors;
  }

  /**
   * Return an actor from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Actor
   */
  static function fromResponse($data) {
    $data = $data['actor'];

    switch ($data['__typename']) {
      case 'Person':
        return Person::fromResponse(['person' => $data]);
      case 'Organisation':
        return Organization::fromResponse(['organisation' => $data]);
    }
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'displayName' => $this->getDisplayName(),
      'avatar' => $this->getAvatar(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate(),
      'facebookUri' => $this->getFacebookUri(),
      'twitterUri' => $this->getTwitterUri(),
      'instagramUri' => $this->getInstagramUri(),
      'websiteUri' => $this->getWebsiteUri()
    ];
  }
}

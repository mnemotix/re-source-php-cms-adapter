<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;
use ReSourceAdapter\Helpers\ListQueryParams;

class ExternalLink extends ModelAbstract {
  /** @var string link name */
  protected $name;

  /** @var string link Uri */
  protected $link;

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getLink() {
    return $this->link;
  }

  /**
   * Get event from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\ExternalLink
   */
  static function fromResponse($data) {
    $data = $data['externalLink'];

    $externalLink = new ExternalLink();

    foreach ($data as $property => $value) {
      $externalLink->{$property} = $value;
    }

    return $externalLink;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'name' => $this->getName(),
      'link' => $this->getLink()
    ];
  }
}

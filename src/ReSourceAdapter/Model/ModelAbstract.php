<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

abstract class ModelAbstract implements \JsonSerializable {
  /** @var $id */
  protected $id;
  /** @var $creationDate */
  protected $creationDate;
  /** @var $lastUpdate */
  protected $lastUpdate;

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  public function getCreationDate() {
    return $this->creationDate;
  }

  public function getLastUpdate() {
    return $this->lastUpdate;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate()
    ];
  }
}

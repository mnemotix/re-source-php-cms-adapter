<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;
use ReSourceAdapter\Helpers\ListQueryParams;

class Locality extends ModelAbstract {
  /** @var string link name */
  protected $name;
  /** @var string street 1 */
  protected $street1;
  /** @var string street 2 */
  protected $street2;
  /** @var string postCode */
  protected $postCode;
  /** @var string city */
  protected $city;
  /** @var string country */
  protected $countryName;

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getStreet1() {
    return $this->street1;
  }

  /**
   * @return string
   */
  public function getStreet2() {
    return $this->street2;
  }

  /**
   * @return string
   */
  public function getPostCode() {
    return $this->postCode;
  }

  /**
   * @return string
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @return string
   */
  public function getCountryName() {
    return $this->countryName;
  }

  /**
   * Get event from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Locality
   */
  static function fromResponse($data) {
    $data = $data['locality'];

    $locality = new Locality();

    foreach ($data as $property => $value) {
      $locality->{$property} = $value;
    }

    return $locality;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'street1' => $this->getStreet1(),
      'street2' => $this->getStreet2(),
      'postCode' => $this->getPostCode(),
      'city' => $this->getCity(),
      'country' => $this->getCountryName()
    ];
  }
}

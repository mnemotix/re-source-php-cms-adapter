<?php
/**
 * This file is part of the lafayette-anticipations package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2017
 */

namespace ReSourceAdapter\Tests;

use PHPUnit\Framework\TestCase;
use ReSourceAdapter\Resolver\Client;

/**
 * @group integration
 *
 * Class ClientTest
 * @package ReSourceAdapter\Tests
 */
class ClientTest extends TestCase {

  /** @var Client */
  protected $client;

  public function setUp() {
    $this->client = new Client();
  }

  public function tearDown() {
    unset($this->client);
  }


  /**
   * @group integration
   */
  public function testConnection(){
    $this->client->refreshToken();

    $this->assertNotEmpty($this->client->getOauthAccessToken());
    $this->assertNotEmpty($this->client->getOauthRefreshToken());
  }

  public function testQuery(){
    $response = $this->client->resolveQuery(<<<QL
query {
  projects(first: 3){edges{node{id}}}
}
QL
 );

    $this->assertCount(3, $response['projects']['edges']);
  }

  /**
   * @group integration
   * @expectedException \ReSourceAdapter\Resolver\GraphQLResponseError
   */
  public function testError(){
    $response = $this->client->resolveQuery(<<<QL
query {
  unknownField
}
QL
    );
  }
}

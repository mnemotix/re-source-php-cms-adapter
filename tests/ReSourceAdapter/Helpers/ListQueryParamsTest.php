<?php
/**
 * This file is part of the lafayette-anticipations package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2017
 */

namespace ReSourceAdapter\Model;

use PHPUnit\Framework\TestCase;
use ReSourceAdapter\Helpers\ListQueryParams;

class ListQueryParamsTest extends TestCase {
  public function testGraphQLize(){
    $params = ListQueryParams::fromArray([
      'qs' => 'Toutou',
      'first' => 10,
      'after' => 4,
      'sortBy' => 'title',
      'sortDirection' => 'asc',
      'filters' => ['public:true']
    ]);

    $this->assertEquals('first: 10, after: "YXJyYXljb25uZWN0aW9uOjQ=", qs: "Toutou", sortBy: "title", sortDirection: "asc", filters: ["public:true"]', $params->graphQLize());
  }
}

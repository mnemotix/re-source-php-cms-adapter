Adaptateur PHP pour Re-Source
-----------------------------

Cette librairie est écrite par (Mnemotix)[http://www.mnemotix.com].

Son but est de mettre à disposition une interface PHP connectée à l'API du fond Re-Source.

Cette bibliothèque utilise le (composer)[https://getcomposer.org/] PHP pour générer ses dépendances. Si l'application utilisant cette librairie


## Installation dans un projet utilisant le Composer PHP

Modifier le fichier `composer.json` pour ajouter la dépendance :

```json
{
    "repositories": [
        {
            "type": "git",
            "url": "https://bitbucket.org/mnemotix/re-source-php-cms-adapter.git"
        }
    ],
    "require": {
        "mnemotix/re-source-php-cms-adapter": "*"
    }
}
```

## Installation dans un projet n'utilisant pas le Composer PHP

- Télécharger les sources de l'adapateur dans le dossier de votre choix, disons `[PROJET]/re-source-php-cms-adapter`.
- Dans ce dossier, installer le composer 

```bash
cd [PROJET]/re-source-php-cms-adapter
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

- Lancer l'installation du composer 

```bash
php composer.phar install
```

- Ajouter la référence à l'autoloader au début des sources du projet.

```php
<?php

require_once('[PROJET]/re-source-php-cms-adapter/vendor/autoload.php');
```



## Tests unitaires

Pour lancer les tests unitaires :

```bash
$> php composer.phar tests
```

## Initialisation

Pour utiliser la librairie, il faut au préalable renseigner la variable d'environnement renseignant la clé d'API.

- `RESOURCE_API_KEY`

D'autres variables peuvent être initialisées :

- `RESOURCE_ENDPOINT_URI` : URI de l'API Re-Source (par défaut https://prod-resource-weever.mnemotix.com/api)
- `RESOURCE_AUTH_URI` : URI du serveur d'authentification de Re-Source (par défaut https://prod-resource-auth.mnemotix.com/auth/realms/synaptix/protocol/openid-connect/token)

## Utilisation

Une fois les variables d'environnement initialisées, il suffit de créer une instance de l'adapter :

```php
<?php

use ReSourceAdapter\Adapter;

$resourceAdapter = new Adapter();
```

### API de données

Puis utiliser une des méthodes permettant de retrouver les données de Re-Source

- Chercher une liste d'acteurs

```php
<?php

/**
* Find actors list
*
* @param string $qs Query string
* @param int $first Number of returned actors
* @param int $after Id of the first returned actor. Default to null.
*
* @return \ReSourceAdapter\Model\Actor[]
*/
$resourceAdapter->findActors($qs, $first, $after);
```

- Chercher une liste d'expositions

```php
<?php
/**
* Find exhibitions list.
*
* @param string $qs Query string
* @param int $first Number of returned exhibitions
* @param int $after Offset of the first return exhibition. Default to null.
*
* @return \ReSourceAdapter\Model\Exhibition[]
*/
$resourceAdapter->findExhibitions($qs, $first, $after);
```

- Chercher une liste d'oeuvre d'une exposition

```php
<?php
/**
* Find artworks list for an exhibition.
*
* @param string $exhibitionId Exhibition id
* @param string $qs Query string
* @param int $first Number of returned artworks
* @param int $after Offser of the first returned artwork. Default to null.
*
* @return \ReSourceAdapter\Model\Artwork[]
*/
$resourceAdapter->findExhibitionArtworks($exhibitionId, $qs, $first, $after);
```

- Chercher une liste d'événements d'une exposition 

```php
<?php
/**
* Find events list for an exhibition.
*
* @param string $exhibitionId Exhibition id
* @param string $qs Query string
* @param int $first Number of returned artworks
* @param int $after Offset of the first returned artwork. Default to null.
*
* @return \ReSourceAdapter\Model\Event[]
*/
$resourceAdapter->findExhibitionEvents($exhibitionId, $qs, $first, $after);
```

- Chercher une liste d'événements d'une oeuvre 

```php
<?php
/**
* Find events list for an artwork.
*
* @param string $artworkId Exhibition id
* @param string $qs Query string
* @param int $first Number of returned artworks
* @param int $after Offset of the first returned artwork. Default to null.
*
* @return \ReSourceAdapter\Model\Event[]
*/
$resourceAdapter->findArtworkEvents($artworkId, $qs, $first, $after);
```

- Retrouver les informations d'un acteur

```php
<?php
/**
* Get actor by id.
*
* @param string $id Actor id
*
* @return \ReSourceAdapter\Model\Actor
*/
$resourceAdapter->getActor($id);
```

- Retrouver les informations d'une exposition

```php
<?php
/**
* Get exhibition by id.
*
* @param string $id Exhibition id
*
* @return \ReSourceAdapter\Model\Exhibition
*/
$resourceAdapter->getExhibition($id);
```

- Retrouver les informations d'une oeuvre

```php
<?php
/**
* Get artwork by id.
*
* @param string $id Artwork id
*
* @return \ReSourceAdapter\Model\Artwork
*/
$resourceAdapter->getArtwork($id);
```

- Retrouver les informations d'un événement

```php
<?php
/**
* Get event by id.
*
* @param string $id Event id
*
* @return \ReSourceAdapter\Model\Event
*/
$resourceAdapter->getEvent($id);
```

- Retrouver les informations d'une ressource

```php
<?php

/**
* Get resource by id.
*
* @param string $id Resource id
*
* @return \ReSourceAdapter\Model\Resource
*/
$resourceAdapter->getResource($id);
```

### API sur le différentiel des ressources.

Pour retrouver un différentiel de ressources, il suffit d'utiliser la méthode :

```php
<?php

/**
   * Get resources differential since.
   *
   * @param float $date Timestamp from which resources have been modified.
   * @param int $first Number of returned resource activity objects
   * @param int $after Offset of the first returned resource activity object. Default to null.   *
   * 
   * @return \ReSourceAdapter\Model\ResourceActivity[]
   */
  $resourceAdapter->getResourcesDifferentialSince($date, $first, $after);
```

Cette méthode retourne une liste d'objets ``\ReSourceAdapter\Model\ResourceActivity`` défini comme suit :

```php
<?php

class ResourceActivity {
  /** @var \ReSourceAdapter\Model\Resource Resource */
  protected $resource;

  /** @var string Action among the list CREATED, DELETED, UPDATED */
  protected $action;

  /**
   * @return \ReSourceAdapter\Model\Resource
   */
  public function getResource() {
    return $this->resource;
  }

  /**
   * @return string
   */
  public function getAction() {
    return $this->action;
  }
}
```

## Exemples

Des exemples d'utilisation peuvent être trouver dans le dossier `examples`.

Pour les faire fonctionner, il est possible d'utiliser le serveur built-in PHP en passant la clé d'API en variable d'environnement :

```bash
RESOURCE_API_KEY=[API KEY] php -S localhost:8000
```



<?php
/**
 * This file is part of the re-source-drupal-adapter package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 11/05/2017
 */

require "../vendor/autoload.php";

use ReSourceAdapter\Adapter;

$adapter = new Adapter();

$event = $adapter->getExhibition($_GET["id"]);

header("Content-type:application/json");
echo json_encode($event, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
